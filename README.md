# dvdpJavaLibraryDownload

The purpose of this program is to easily download a Library hosted on a maven repository together with it's dependencies.

The reason for this miniproject is that (1) I often used maven just for it's ability to download and install libraries and its dependencies, but (2) maven and eclipse do not always play well together, and (3) I experienced sessions of issues where there were issues with an advanced java version, jpms and maven.

Another reason is that Maven is powerfull and performs a lot tasks, when that complexity becomes 'magic' and results in too many searches on  stackoverflow it is sometimes good to fall back on the 'standard' java tooling, with some simple scripting.


Objectives:
- [ ] Be able to download the the Sparkjava library jar-files and its mandatory dependencies from a public maven repository (https://mvnrepository.com/)
- [ ] Be able to download the the vertX library jar-files and its mandatory dependencies from a public maven repository (https://mvnrepository.com/)
- [ ] Be able to download javadoc and source files for sparkjava/source
- [ ] Use as part of JShell
- [ ] Run as interactive CLI

Some other 
- Limit use of external libraries (i.e. use JaveSE libraries only)
- Target Java 17
- Have some fun!




