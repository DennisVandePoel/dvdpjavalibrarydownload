package info.dvdp.mvnDwnld;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestMvnDwnld {
    static MvnDwnld aMe;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
	aMe = new MvnDwnld();
    }

    @Test
    final void testTestMe() {
	var me = new MvnDwnld();
	assertEquals("Test me", me.testMe());
    }
    
    @Test
    final void anotherTest() {
	assertNotEquals("Test me", aMe.testMe());
    }

}
